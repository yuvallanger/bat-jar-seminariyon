[ ] "Agonistic encounters"? page 2
[ ] "anechoic chambre" - one that does not produce echoes. page 2

# Article title
    doi:

## Research question

## Subjects

15 male and 5 female Mexican free tailed bats.
* Bat Genus species
* Number
* Sex
* naive or trained

## Methods

* Field or lab
* Tools
* Procedure
* Manipulation

### Results

* Sound frequency
* Calls frequency
* Power
* Duration
* Flight
