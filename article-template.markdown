# Article title
    doi:

## Research question

## Subjects

* Bat Genus species
* Number
* Sex
* naive or trained

## Methods

* Field or lab
* Tools
* Procedure
* Manipulation

### Results

* Sound frequency
* Calls frequency
* Power
* Duration
* Flight
